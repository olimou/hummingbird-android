package com.olimou.hummingbird.http;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.olimou.hummingbird.http.model.Movie;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

import static android.content.ContentValues.TAG;

/**
 * Created by emerson on 14/05/17.
 */
@RunWith(AndroidJUnit4.class)
public class HttpRequestTest {

	@Test
	public void searchMovie() throws Exception {
		final CountDownLatch lSignal = new CountDownLatch(1);

		Context lTargetContext = InstrumentationRegistry.getTargetContext();

		HttpRequest lHttpRequest = new HttpRequest();

		lHttpRequest.searchMovie(lTargetContext, "Home aranha", 1, new HttpRequest.HTTPListener() {

			@Override
			public void error(NetworkResponse _networkResponse) {
				if (_networkResponse.data != null) {
					Log.d(TAG, "error: Error" + Arrays.toString(_networkResponse.data));
				} else {
					Log.d(TAG, "error: Error");
				}

				Assert.assertTrue(false);

				lSignal.countDown();
			}

			@Override
			public void success(Movie _movie) {
				Assert.assertTrue(_movie.getResults().length > 0);

				lSignal.countDown();
			}
		});

		lSignal.await();
	}
}