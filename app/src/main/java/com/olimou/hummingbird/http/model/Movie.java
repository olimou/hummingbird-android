package com.olimou.hummingbird.http.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Movie implements Parcelable {
	public static final Creator<Movie> CREATOR = new Creator<Movie>() {
		@Override
		public Movie createFromParcel(Parcel source) {
			Movie var = new Movie();
			var.page = source.readInt();
			var.total_pages = source.readInt();
			var.results = source.createTypedArray(MovieResults.CREATOR);
			var.total_results = source.readInt();
			return var;
		}

		@Override
		public Movie[] newArray(int size) {
			return new Movie[size];
		}
	};
	private int            page;
	private int            total_pages;
	private MovieResults[] results;
	private int            total_results;

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal_pages() {
		return this.total_pages;
	}

	public void setTotal_pages(int total_pages) {
		this.total_pages = total_pages;
	}

	public MovieResults[] getResults() {
		return this.results;
	}

	public void setResults(MovieResults[] results) {
		this.results = results;
	}

	public int getTotal_results() {
		return this.total_results;
	}

	public void setTotal_results(int total_results) {
		this.total_results = total_results;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.page);
		dest.writeInt(this.total_pages);
		dest.writeTypedArray(this.results, flags);
		dest.writeInt(this.total_results);
	}

	@Override
	public int describeContents() {
		return 0;
	}
}
