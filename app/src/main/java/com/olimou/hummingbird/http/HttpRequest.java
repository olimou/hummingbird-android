package com.olimou.hummingbird.http;

import android.content.Context;
import android.util.Log;

import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.olimou.hummingbird.R;
import com.olimou.hummingbird.http.model.Movie;

import java.util.Locale;

import static android.content.ContentValues.TAG;

/**
 * Created by emerson on 14/05/17.
 */

public class HttpRequest {

	public static final String URL_DISCOVER = "https://api.themoviedb.org/3/discover";
	public static final String URL_SEARCH   = "https://api.themoviedb.org/3/search";
	private RequestQueue mRequestQueue;

	public void cancel() {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll("search");
		}
	}

	public void searchDiscover(Context _context, int _page, final HTTPListener _httpListener) {
		// Instantiate the RequestQueue.
		mRequestQueue = Volley.newRequestQueue(_context);

		String lUrl = String.format(Locale.getDefault(), "%s/movie?api_key=%s&language=%s&page=%d",
				URL_DISCOVER, _context.getString(R.string.themovieappdb_key),
				_context.getString(R.string.themovieappdb_language), _page);

		// Request a string response from the provided URL.
		StringRequest lStringRequest = new StringRequest(Request.Method.GET, lUrl,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String _response) {
						Movie lMovie = new Gson().fromJson(_response, Movie.class);

						_httpListener.success(lMovie);
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				_httpListener.error(error.networkResponse);
			}
		});

		// Add the request to the RequestQueue.
		lStringRequest.setTag("search");

		mRequestQueue.add(lStringRequest);
	}

	public void searchMovie(Context _context,
	                        String _search,
	                        int _page,
	                        final HTTPListener _httpListener) {
		// Instantiate the RequestQueue.
		mRequestQueue = Volley.newRequestQueue(_context);

		String lUrl = String
				.format(Locale.getDefault(), "%s/movie?api_key=%s&language=%s&query=%s&page=%d",
						URL_SEARCH, _context.getString(R.string.themovieappdb_key),
						_context.getString(R.string.themovieappdb_language), _search, _page);

		Log.d(TAG, "searchMovie: " + lUrl);

		// Request a string response from the provided URL.
		StringRequest lStringRequest = new StringRequest(Request.Method.GET, lUrl,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String _response) {
						Movie lMovie = new Gson().fromJson(_response, Movie.class);

						_httpListener.success(lMovie);
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				_httpListener.error(error.networkResponse);
			}
		});

		// Add the request to the RequestQueue.
		lStringRequest.setTag("search");

		mRequestQueue.add(lStringRequest);
	}

	public interface HTTPListener {
		void error(NetworkResponse _networkResponse);

		void success(Movie _movie);
	}
}
