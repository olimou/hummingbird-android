package com.olimou.hummingbird;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.util.AttributeSet;

/**
 * Created by EmersonMoura on 15/05/17.
 */

public class CustomSearch extends SearchView {
	private boolean mExpanded;

	public CustomSearch(Context context) {
		super(context);

		init();
	}

	public CustomSearch(Context context, AttributeSet attrs) {
		super(context, attrs);

		init();
	}

	public CustomSearch(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		init();
	}

	private void init() {
		setIconifiedByDefault(true);
	}

	public boolean isExpanded() {
		return mExpanded;
	}

	@Override
	public void onActionViewCollapsed() {
		super.onActionViewCollapsed();

		mExpanded = false;
	}

	@Override
	public void onActionViewExpanded() {
		super.onActionViewExpanded();

		mExpanded = true;
	}
}
