package com.olimou.hummingbird;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent lIntent = new Intent(SplashActivity.this, MainActivity.class);

		startActivity(lIntent);

		finish();
	}
}