package com.olimou.hummingbird.image;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by emerson on 14/05/17.
 */

public class ImageHttp extends AppCompatImageView {

	public final String TAG = this.getClass().getName();
	private AsyncTask<LoadParams, Integer, Bitmap> mExecute;

	public ImageHttp(Context context) {
		super(context);
	}

	public ImageHttp(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ImageHttp(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@BindingAdapter({"app:imageUrl"})
	public static void loadImage(ImageHttp _imageHttp, String _imageUrl) {
		_imageHttp.loadURL(_imageUrl);
	}

	public void loadURL(final String _url) {
		if (mExecute != null) {
			mExecute.cancel(true);
		}

		setImageBitmap(null);

		if (_url != null && _url.length() > 0) {
			post(new Runnable() {
				@Override
				public void run() {
					int lWidth = getWidth();

					int lHeight = getHeight();

					LoadParams lLoadParams = new LoadParams(lWidth, lHeight, _url);

					mExecute = new LoadFromUrl().execute(lLoadParams);
				}
			});
		}
	}

	class LoadFromUrl extends AsyncTask<LoadParams, Integer, Bitmap> {

		// https://developer.android.com/topic/performance/graphics/load-bitmap.html
		public int calculateInSampleSize(Bitmap _bitmap, int _reqWidth, int _reqHeight) {
			// Raw height and width of image
			final int lHeight = _bitmap.getHeight();
			final int lWidth = _bitmap.getWidth();

			int lInSampleSize = 1;

			if (lHeight > _reqHeight || lWidth > _reqWidth) {

				final int lHalfHeight = lHeight / 2;
				final int lHalfWidth = lWidth / 2;

				// Calculate the largest inSampleSize value that is a power of 2 and keeps both
				// height and width larger than the requested height and width.
				while ((lHalfHeight / lInSampleSize) >= _reqHeight && (lHalfWidth / lInSampleSize) >= _reqWidth) {
					lInSampleSize *= 2;
				}
			}

			return lInSampleSize;
		}

		@Override
		protected Bitmap doInBackground(LoadParams... params) {
			LoadParams lParam = params[0];

			Bitmap lBitmap = getBitmap(lParam.getUrl());

			if (lBitmap != null) {
				int lSampleSize = calculateInSampleSize(lBitmap, lParam.getWidth(),
						lParam.getHeight());

				int lDstWidth = lBitmap.getWidth() / lSampleSize;
				int lDstHeight = lBitmap.getHeight() / lSampleSize;

				if (lDstHeight > 1 || lDstWidth > 1) {
					return Bitmap.createScaledBitmap(lBitmap, lDstWidth, lDstHeight, false);
				}

				return lBitmap;
			}

			return null;
		}

		private Bitmap getBitmap(String _url) {
			try {
				InputStream lInputStream = (InputStream) new URL(_url).getContent();

				Bitmap lBitmap = BitmapFactory.decodeStream(lInputStream);

				lInputStream.close();

				return lBitmap;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(Bitmap _bitmap) {
			super.onPostExecute(_bitmap);

			setImageBitmap(_bitmap);
		}
	}

	public class LoadParams {
		private int    mHeight;
		private String mUrl;
		private int    mWidth;

		public LoadParams(int _width, int _height, String _url) {
			mWidth = _width;
			mHeight = _height;
			mUrl = _url;
		}

		public int getHeight() {
			return mHeight;
		}

		public void setHeight(int _height) {
			mHeight = _height;
		}

		public String getUrl() {
			return mUrl;
		}

		public void setUrl(String _url) {
			mUrl = _url;
		}

		public int getWidth() {
			return mWidth;
		}

		public void setWidth(int _width) {
			mWidth = _width;
		}
	}
}
