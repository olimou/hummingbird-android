package com.olimou.hummingbird.movies.details;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.olimou.hummingbird.R;
import com.olimou.hummingbird.databinding.ActivityMovieDetailsBinding;
import com.olimou.hummingbird.http.model.MovieResults;

public class MovieDetailsActivity extends AppCompatActivity {

	public static final String EXTRA_MOVIE = "MOVIE";
	private ActivityMovieDetailsBinding mBinding;

	public static void create(Fragment _fragment, MovieResults _movie) {
		Bundle lBundle = new Bundle();
		lBundle.putParcelable(EXTRA_MOVIE, _movie);

		Intent lIntent = new Intent(_fragment.getContext(), MovieDetailsActivity.class);
		lIntent.putExtras(lBundle);
		_fragment.startActivity(lIntent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mBinding = DataBindingUtil.setContentView(this, R.layout.activity_movie_details);

		MovieResults lMovieResults = getIntent().getExtras().getParcelable(EXTRA_MOVIE);

		setupToolbar(lMovieResults);

		MovieDetailsActivityModel lModel = new MovieDetailsActivityModel(lMovieResults);

		mBinding.setViewModel(lModel);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
		}

		return super.onOptionsItemSelected(item);
	}

	private void setupToolbar(MovieResults _movieResults) {
		setSupportActionBar(mBinding.toolbar);

		ActionBar lSupportActionBar = getSupportActionBar();

		if (lSupportActionBar != null && _movieResults != null) {
			lSupportActionBar.setTitle(_movieResults.getTitle());
			lSupportActionBar.setDisplayHomeAsUpEnabled(true);
		} else {
			finish();
		}
	}
}
