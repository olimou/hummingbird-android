package com.olimou.hummingbird.movies.list.adapter;

import android.databinding.BaseObservable;

import com.olimou.hummingbird.http.model.MovieResults;

/**
 * Created by emerson on 14/05/17.
 */

public class MovieViewModel extends BaseObservable {
	private MovieResults movie;

	public MovieViewModel(MovieResults _movie) {
		movie = _movie;
	}

	public String getCover() {
		String lPoster_path = movie.getPoster_path();

		if (lPoster_path != null) {
			return "https://image.tmdb.org/t/p/w500" + lPoster_path;
		} else {
			return "";
		}
	}

	public MovieResults getMovie() {
		return movie;
	}

	public String getYear() {
		String lRelease_date = movie.getRelease_date();

		if (lRelease_date.length() > 5) {
			return lRelease_date.substring(0, 4);
		}

		return "";
	}
}
