package com.olimou.hummingbird.movies.list.adapter;

import com.olimou.hummingbird.http.model.MovieResults;

/**
 * Created by EmersonMoura on 15/05/17.
 */

public interface MovieViewListener {
	void click(MovieResults _movie);
}
