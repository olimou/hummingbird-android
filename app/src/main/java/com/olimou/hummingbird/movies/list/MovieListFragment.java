package com.olimou.hummingbird.movies.list;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.view.*;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.olimou.hummingbird.CustomSearch;
import com.olimou.hummingbird.R;
import com.olimou.hummingbird.databinding.FragmentMovieListBinding;
import com.olimou.hummingbird.http.HttpRequest;
import com.olimou.hummingbird.http.model.Movie;
import com.olimou.hummingbird.http.model.MovieResults;
import com.olimou.hummingbird.movies.details.MovieDetailsActivity;
import com.olimou.hummingbird.movies.list.adapter.MovieViewListener;

import java.util.Arrays;
import java.util.List;

public class MovieListFragment extends Fragment
		implements ListMoviesAdapter.PaginationListener, HttpRequest.HTTPListener,
		MovieViewListener {

	public static final String TAG = MovieListFragment.class.getSimpleName();

	private ListMoviesAdapter        mAdapter;
	private FragmentMovieListBinding mBinding;
	private Handler                  mHandler;
	private HttpRequest              mHttpRequest;
	private MovieListFragmentModel   mListFragmentModel;
	private String                   mSearch;

	public MovieListFragment() {

	}

	private void cancelSearch() {
		mHandler.removeCallbacksAndMessages(null);
	}

	@Override
	public void click(MovieResults _movie) {
		MovieDetailsActivity.create(this, _movie);
	}

	@Override
	public void error(NetworkResponse _networkResponse) {
		mBinding.swipeLayout.setRefreshing(false);

		Toast.makeText(getContext(), R.string.there_was_an_error, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		inflater.inflate(R.menu.menu_search, menu);

		final MenuItem lMenuSearchItem = menu.findItem(R.id.search);

		Drawable lIcon = lMenuSearchItem.getIcon();

		lIcon.mutate().setColorFilter(ContextCompat.getColor(getContext(), R.color.white),
				PorterDuff.Mode.SRC_ATOP);

		setupSearchMenu(lMenuSearchItem);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		mBinding = FragmentMovieListBinding.inflate(inflater, container, false);

		mListFragmentModel = new MovieListFragmentModel();

		mBinding.setViewModel(mListFragmentModel);

		setHasOptionsMenu(true);

		mHandler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				onLoad(1);

				return true;
			}
		});

		mHttpRequest = new HttpRequest();

		mAdapter = new ListMoviesAdapter(this, this);

		mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		mBinding.recyclerView.setAdapter(mAdapter);

		mBinding.swipeLayout
				.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorPrimary));

		mBinding.swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				onLoad(1);
			}
		});

		setToolbarTitle(null);

		onLoad(1);

		return mBinding.getRoot();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (mHttpRequest != null) {
			mHttpRequest.cancel();
		}
	}

	@Override
	public void onLoad(int _index) {
		final SwipeRefreshLayout lSwipeLayout = mBinding.swipeLayout;

		lSwipeLayout.setRefreshing(true);

		mAdapter.setIndex(_index);

		if (mSearch != null && mSearch.length() > 0) {
			mHttpRequest.searchMovie(getContext(), mSearch, _index, this);
		} else {
			mHttpRequest.searchDiscover(getContext(), _index, this);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				mSearch = null;

				setToolbarTitle(null);

				updateSearch();

				break;
		}

		return super.onOptionsItemSelected(item);
	}

	private void setToolbarTitle(String _query) {
		AppCompatActivity lActivity = (AppCompatActivity) getActivity();

		ActionBar lActionBar = lActivity.getSupportActionBar();

		if (lActionBar != null) {
			if (_query != null) {
				lActionBar.setDisplayHomeAsUpEnabled(true);
				lActionBar.setTitle(_query);
			} else {
				lActionBar.setDisplayHomeAsUpEnabled(false);
				lActionBar.setTitle("Popular");
				mListFragmentModel.setMode(MovieListFragmentModel.MODE_SEARCH_NORMAL);
			}
		}
	}

	private void setupSearchMenu(final MenuItem _menuSearchItem) {
		final SearchManager lSearchManager =
				(SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

		final CustomSearch lSearchView = (CustomSearch) _menuSearchItem.getActionView();

		lSearchManager.setOnCancelListener(new SearchManager.OnCancelListener() {
			@Override
			public void onCancel() {
				setToolbarTitle(null);
			}
		});

		lSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				if (newText.length() > 0) {
					mSearch = newText;

					updateSearch();
				} else {
					cancelSearch();
				}

				return true;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				setToolbarTitle(query);

				MenuItemCompat.collapseActionView(_menuSearchItem);

				return true;
			}
		});

		lSearchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
			@Override
			public void onViewAttachedToWindow(View v) {
				mListFragmentModel.setMode(MovieListFragmentModel.MODE_SEARCH_START);
			}

			@Override
			public void onViewDetachedFromWindow(View v) {
				setToolbarTitle(mSearch);
			}
		});

		ComponentName lComponentName = getActivity().getComponentName();

		lSearchView.setSearchableInfo(lSearchManager.getSearchableInfo(lComponentName));
	}

	@Override
	public void success(Movie _movie) {
		mBinding.swipeLayout.setRefreshing(false);

		List<MovieResults> lMovieResultsList = Arrays.asList(_movie.getResults());

		mAdapter.setItems(lMovieResultsList);

		int lPage = _movie.getPage();

		mAdapter.setPage(lPage, _movie.getTotal_pages());

		if (lPage == 1) {
			mBinding.recyclerView.scrollToPosition(0);
		}

		if (_movie.getResults().length > 0) {
			mListFragmentModel.setMode(MovieListFragmentModel.MODE_SEARCH_NORMAL);
		} else {
			mListFragmentModel.setMode(MovieListFragmentModel.MODE_SEARCH_NULL);
		}
	}

	private void updateSearch() {
		cancelSearch();

		mHandler.sendEmptyMessageDelayed(0, 400);
	}
}
