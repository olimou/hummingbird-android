package com.olimou.hummingbird.movies.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.olimou.hummingbird.databinding.AdapterMovieBinding;
import com.olimou.hummingbird.http.model.MovieResults;

/**
 * Created by emerson on 14/05/17.
 */

public class MovieViewHolder extends RecyclerView.ViewHolder {

	public final String TAG = this.getClass().getSimpleName();

	private AdapterMovieBinding mBinding;

	public MovieViewHolder(AdapterMovieBinding _binding) {
		super(_binding.getRoot());

		mBinding = _binding;
	}

	public void setup(final MovieResults _movie, final MovieViewListener _movieViewListener) {
		MovieViewModel lViewModel = new MovieViewModel(_movie);

		mBinding.setViewModel(lViewModel);

		mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (_movieViewListener != null) {
					_movieViewListener.click(_movie);
				}
			}
		});

		mBinding.executePendingBindings();
	}
}
