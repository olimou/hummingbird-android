package com.olimou.hummingbird.movies.details;

import android.databinding.BaseObservable;

import com.olimou.hummingbird.http.model.MovieResults;

/**
 * Created by EmersonMoura on 15/05/17.
 */

public class MovieDetailsActivityModel extends BaseObservable {
	private MovieResults movie;

	public MovieDetailsActivityModel(MovieResults _movieResults) {
		movie = _movieResults;
	}

	public String getCover() {
		String lPoster_path = movie.getBackdrop_path();

		if (lPoster_path != null) {
			return "https://image.tmdb.org/t/p/w500" + lPoster_path;
		} else {
			return "";
		}
	}

	public MovieResults getMovie() {
		return movie;
	}
}
