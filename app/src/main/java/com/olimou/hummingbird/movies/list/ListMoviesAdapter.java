package com.olimou.hummingbird.movies.list;

import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.olimou.hummingbird.databinding.AdapterMovieBinding;
import com.olimou.hummingbird.http.model.MovieResults;
import com.olimou.hummingbird.movies.list.adapter.MovieViewHolder;
import com.olimou.hummingbird.movies.list.adapter.MovieViewListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emerson on 14/05/17.
 */

public class ListMoviesAdapter extends RecyclerView.Adapter<MovieViewHolder> {
	private int                mIndex;
	private List<MovieResults> mMovieResults;
	private MovieViewListener  mMovieViewListener;
	private PaginationListener mPaginationListener;

	public ListMoviesAdapter(PaginationListener _paginationListener,
	                         MovieViewListener _movieViewListener) {
		mMovieViewListener = _movieViewListener;
		mMovieResults = new ArrayList<>();
		mPaginationListener = _paginationListener;

	}

	@Override
	public int getItemCount() {
		return mMovieResults.size();
	}

	@Override
	public void onBindViewHolder(MovieViewHolder holder, int position) {
		holder.setup(mMovieResults.get(position), mMovieViewListener);

		if (position == getItemCount() - 1 && mIndex > 0) {
			new Handler(new Handler.Callback() {
				@Override
				public boolean handleMessage(Message msg) {
					if (mPaginationListener != null) {
						mPaginationListener.onLoad(mIndex);
					}
					return true;
				}
			}).sendEmptyMessage(0);
		}
	}

	@Override
	public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater lLayoutInflater = LayoutInflater.from(parent.getContext());

		AdapterMovieBinding lBinding = AdapterMovieBinding.inflate(lLayoutInflater, parent, false);

		return new MovieViewHolder(lBinding);
	}

	public void setIndex(int _index) {
		mIndex = _index;
	}

	public void setItems(List<MovieResults> _items) {
		if (mIndex == 1) {
			mMovieResults.clear();
			mMovieResults.addAll(_items);
		} else {
			mMovieResults.addAll(_items);
		}

		notifyDataSetChanged();
	}

	public void setPage(int _page, int _total_pages) {
		if (_page < _total_pages) {
			mIndex = _page + 1;
		} else {
			mIndex = -1;
		}
	}

	public interface PaginationListener {
		void onLoad(int _index);
	}
}