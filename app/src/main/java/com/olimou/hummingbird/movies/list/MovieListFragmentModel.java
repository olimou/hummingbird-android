package com.olimou.hummingbird.movies.list;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;

/**
 * Created by EmersonMoura on 15/05/17.
 */

public class MovieListFragmentModel extends BaseObservable {
	public static final int MODE_SEARCH_NORMAL = 3;
	public static final int MODE_SEARCH_NULL   = 2;
	public static final int MODE_SEARCH_START  = 1;

	public static final String TAG = MovieListFragmentModel.class.getSimpleName();

	int mMode;

	@Bindable
	public int getSearchNormal() {
		if (mMode == MODE_SEARCH_NORMAL) {
			return View.VISIBLE;
		}

		return View.GONE;
	}

	@Bindable
	public int getSearchNull() {
		if (mMode == MODE_SEARCH_NULL) {
			return View.VISIBLE;
		}

		return View.GONE;
	}

	@Bindable
	public int getSearchStart() {
		if (mMode == MODE_SEARCH_START) {
			return View.VISIBLE;
		}

		return View.GONE;
	}

	public void setMode(int _mode) {
		mMode = _mode;

		notifyPropertyChanged(BR.searchNull);
		notifyPropertyChanged(BR.searchStart);
		notifyPropertyChanged(BR.searchNormal);
	}
}
