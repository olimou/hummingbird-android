package com.olimou.hummingbird;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.olimou.hummingbird.databinding.ActivityMainBinding;
import com.olimou.hummingbird.movies.list.MovieListFragment;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityMainBinding lBinding =
				DataBindingUtil.setContentView(this, R.layout.activity_main);

		setSupportActionBar(lBinding.toolbar);

		getSupportFragmentManager().beginTransaction().replace(R.id.frame, new MovieListFragment())
				.commit();
	}
}